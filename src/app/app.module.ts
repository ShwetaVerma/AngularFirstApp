import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { TodoComponent } from './todo/todo.component';
import { AddComponent } from './todo/add/add.component';
import { DisplayComponent } from './todo/display/display.component';
import { EditlistComponent } from './todo/editlist/editlist.component';
import { todoService } from './services/todoservice.service';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { CategoryComponent } from './category/category.component';
import { AddcategoryComponent } from './category/addcategory/addcategory.component';
import { DisplaycategoryComponent } from './category/displaycategory/displaycategory.component';
import { EditcategoryComponent } from './category/editcategory/editcategory.component';
import { SubstringPipe } from './substring.pipe';
import { TodostatusfilterPipe } from './todostatusfilter.pipe';

const appRoutes: Routes = [
      { 
        path: '', component: DisplayComponent,
        children:[
                   { path: 'todo/add', component: AddComponent },
                   { path: 'todo/edit/:id', component: EditlistComponent },
                   { path: 'category/add', component: AddcategoryComponent },
                   { path: 'category/edit/:id', component: EditcategoryComponent },
                   { path: 'category', component: DisplaycategoryComponent }
                 ]
       },
       { path: 'register', component: RegisterComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TodoComponent,
    AddComponent,
    DisplayComponent,
    EditlistComponent,
    RegisterComponent,
    CategoryComponent,
    AddcategoryComponent,
    DisplaycategoryComponent,
    EditcategoryComponent,
    SubstringPipe,
    TodostatusfilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
