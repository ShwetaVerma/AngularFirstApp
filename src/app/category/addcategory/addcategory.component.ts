import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { CategoryService} from '../../services/category.service';
import { NgForm } from '../../../../node_modules/@angular/forms';

@Component({
  selector: 'app-addcategory',
  templateUrl: './addcategory.component.html',
  styleUrls: ['./addcategory.component.css']
})
export class AddcategoryComponent implements OnInit {

  onClick(addcategoryForm:NgForm)
  {
  console.log(addcategoryForm)
    if(addcategoryForm.invalid===false)
   {
     this.categoryService.categoryAdded(addcategoryForm.value.categoryName,addcategoryForm.value.categoryID)
   }
   else
   {

   }

  }

  constructor(private categoryService:CategoryService) { }
  ngOnInit() {
  }
 
}

