import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {CategoryService} from '../../services/category.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-displaycategory',
  templateUrl: './displaycategory.component.html',
  styleUrls: ['./displaycategory.component.css']
})
export class DisplaycategoryComponent implements OnInit {
  visible: boolean = false;
  @Input() change;
 textData;
 
   ngOnInit() {
    this.categoryServices.change.subscribe(visible => {
      this.visible = visible;
    });

  }
   toggleBtn(todoIndex){
     console.log(todoIndex)
     this.visible=true;
   this.router.navigate(['/category', 'edit', todoIndex + 1]);
   }
  onClick(indexOfDelete){
    this.categoryServices.itemIndexForDelete(indexOfDelete);
  }
  categoryServices: CategoryService;

  constructor(private router: Router, private route: ActivatedRoute, private CategoryService: CategoryService) {
    this.categoryServices = CategoryService;
  }

}
