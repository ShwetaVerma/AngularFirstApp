import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {CategoryService} from '../../services/category.service';

@Component({
  selector: 'app-editcategory',
  templateUrl: './editcategory.component.html',
  styleUrls: ['./editcategory.component.css']
})
export class EditcategoryComponent implements OnInit {
  @Input() itemEdit;
  @Input() category;
  @ViewChild('name') name: ElementRef;
  @ViewChild('ID') ID: ElementRef;
  
  
 categoryServices: CategoryService;
 
 constructor(private router: Router, private route: ActivatedRoute, private categoryService: CategoryService) { }
 
  ngOnInit() {
    var id = this.route.snapshot.params['id'];
    this.route.params.subscribe((params: Params) => {
      id = params['id'];
      this.category = this.categoryService.getToDo(id);
      this.itemEdit=this.category;
    });
    
  }

btnSave(item:HTMLInputElement)
{
  this.itemEdit.name = this.name.nativeElement.value;
  this.itemEdit.ID= this.ID.nativeElement.value;
}
btnCancel()
{
this.categoryService.CancelEdit();
}

}
