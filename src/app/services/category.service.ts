import { Injectable, Output, EventEmitter } from '@angular/core';
import {Category} from '../models/Category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
 currentId = 0;
 categories:Category[]= [];
 @Output() change: EventEmitter<boolean> = new EventEmitter();
 @Output() item = new EventEmitter< {name: string, ID: number }>();
checkingValue;
 visible: boolean = false;
 EditIndex;
indexForEdit;
 constructor() { 
   this.addNewCategory('Category 1', 1),
   this.addNewCategory('Category 2', 2),
   this.addNewCategory('Category 3', 3)
 }
 
 addNewCategory(name: string, ID: number) {
   this.currentId++;
   this.categories.push(
     new Category(this.currentId,name,ID)
   );

 }
 categoryAdded(name:string,ID:number) {
   this.addNewCategory(name, ID);
 }
  newItemAdded(newitem) {
   this.categories.push(newitem);
 }
 itemRemoved(itemTobeDeleted) {
   this.categories.splice(itemTobeDeleted, 1);
 }

 openForEdit(index) {
   this.checkingValue = this.categories[index];
   this.item.emit(this.checkingValue);
   this.EditIndex=index;
 }

 itemIndexForDelete(index){
   this.categories.splice(index, 1);
 }

 ItemToEdit(name,ID){
   this.addEditedToDo(name, ID);
   this.change.emit(this.visible);
 }
 addEditedToDo(name: string, ID: number) {
   this.categories.splice(1,1)
   console.log(status)
   this.categories.push(
     new Category(2, name,2)
   );

 }
 getToDo(id: number): Category {
   return this.categories.find( o => o.id == id );
 }
 CancelEdit()  {
      this.change.emit(this.visible);
 }
}
