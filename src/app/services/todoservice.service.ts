import { Injectable, Output, EventEmitter } from '@angular/core';
import { ToDo  } from '../models/ToDo';
import {Category} from '../models/Category';

@Injectable({
  providedIn: 'root'
})

export class todoService {
  defaultCategory = new Category(1,'Inbox',1);
 // item;
  currentId = 0;
  todos:ToDo[]= [];
  @Output() change: EventEmitter<boolean> = new EventEmitter();
  @Output() item = new EventEmitter< {name: string, status: string }>();
checkingValue;
  visible: boolean = false;
  EditIndex;
indexForEdit;
  constructor() { 
    this.addNewToDo('List 1', "Done"),
    this.addNewToDo('List 2', "Done"),
    this.addNewToDo('List 3', "Not Done")
  }
  
  addNewToDo(name: string, status: string) {
    this.currentId++;
    this.todos.push(
      new ToDo(this.currentId, name, status,this.defaultCategory)
    );

  }
  toDoAdded(name:string,taskStatus:string) {
    this.addNewToDo(name, taskStatus);
  }
   newItemAdded(newitem) {
    //this.servers.push({name: newServer.name, status: newServer.status});
    this.todos.push(newitem);
  }
  itemRemoved(itemTobeDeleted) {
    //this.servers.push({name: newServer.name, status: newServer.status});
    this.todos.splice(itemTobeDeleted, 1);
  }

  openForEdit(index) {
    this.checkingValue = this.todos[index];
    this.item.emit(this.checkingValue);
    this.EditIndex=index;
  }
 
  itemIndexForDelete(index){
    this.todos.splice(index, 1);
  }
 
  ItemToEdit(name,taskstatus){
    this.addEditedToDo(name, taskstatus);
    this.change.emit(this.visible);
  }
  addEditedToDo(name: string, status: string) {
    this.todos.splice(1,1)
    console.log(status)
    this.todos.push(
      new ToDo(2, name, status,this.defaultCategory)
    );

  }
  getToDo(id: number): ToDo {
    // var toDo = this.todos.find((o) => {
    //   return o.id == id;
    // });
    //return toDo;
    return this.todos.find( o => o.id == id );
  }
  CancelEdit()  {
       this.change.emit(this.visible);
  }
 
}

