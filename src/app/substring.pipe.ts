import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'substring'
})
export class SubstringPipe implements PipeTransform {

  transform(value: any, start:number,end:number): any {
    if(value.length>(end-start))
    {
      return value.substr(start,(end-start))+'...';
    }
    else{
      return value;
    }
  }

}
