import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { todoService} from '../../services/todoservice.service';
import { NgForm } from '../../../../node_modules/@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  //@Output() itemAdded = new EventEmitter<{name: string, taskStatus: string}>();
 
 
  public selectedStatus:string='';
  Statuses: any=[
   'Done',
   'Not Done'
 ];
radioChangeHandler(event:any){
  this.selectedStatus=event.target.value;
}

  // onClick(item:HTMLInputElement)
  // {
  //  console.log(item,this.selectedStatus)
  //   // this.itemAdded.emit({
  //   //   name: item.value,
  //   //   taskStatus: this.selectedStatus
  //   // });
  //   this.todoService.toDoAdded(item.value,this.selectedStatus)
  // }
 
  onClick(addtoDoForm:NgForm)
  {
    // this.itemAdded.emit({
    //   name: item.value,
    //   taskStatus: this.selectedStatus
    // });
    if(addtoDoForm.invalid===false)
   {
     this.todoService.toDoAdded(addtoDoForm.value.toDoName,addtoDoForm.value.toDoStatus)
   }
   else
   {

   }

  }

  constructor(private todoService:todoService) { }

  ngOnInit() {
  }
 
}
