// import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {todoService} from '../../services/todoservice.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { NgForm } from '../../../../node_modules/@angular/forms';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {
  
  visible: boolean = false;
  //@Output() itemRemoved = new EventEmitter<{name: string, taskStatus: string}>();
  @Input() change;
   //@Output() itemOpenForEdit = new EventEmitter<number>();
 textData;
 dropdownValue;
 
   ngOnInit() {
    this.toDoServices.change.subscribe(visible => {
      this.visible = visible;
    });

  }
   toggleBtn(todoIndex){
     this.visible=true;
  //   //this.itemOpenForEdit.emit(todoIndex)
  //  this.toDoServices.openForEdit(todoIndex)
  //  this.textData=this.toDoServices.checkingValue;
   this.router.navigate(['/todo', 'edit', todoIndex + 1]);
   }
  onClick(indexOfDelete){
    this.toDoServices.itemIndexForDelete(indexOfDelete);
  }
  dropdown(dropdowntoDoForm:NgForm){
    this.dropdownValue=dropdowntoDoForm.value.statusSelected;
  }

  toDoServices: todoService;

  constructor(private router: Router, private route: ActivatedRoute, private toDoService: todoService) {
    this.toDoServices = toDoService;
  }

}
