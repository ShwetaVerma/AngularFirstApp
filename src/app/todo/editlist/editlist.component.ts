import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {todoService} from '../../services/todoservice.service';

@Component({
  selector: 'app-editlist',
  templateUrl: './editlist.component.html',
  styleUrls: ['./editlist.component.css']
})
export class EditlistComponent implements OnInit {
  // @Output() itemEdited = new EventEmitter<{name: string, taskStatus: string}>();
  @Input() itemEdit;
  @Input() todo;
  @ViewChild('name') name: ElementRef;
  
  public selectedStatus:string='';
  Statuses: any=[
   'Done',
   'Not Done'
 ];
 toDoServices: todoService;
 
 constructor(private router: Router, private route: ActivatedRoute, private toDoService: todoService) { }
 
  ngOnInit() {
    var id = this.route.snapshot.params['id'];
    this.route.params.subscribe((params: Params) => {
      id = params['id'];
      this.todo = this.toDoService.getToDo(id);
      this.itemEdit=this.todo;
    });
    // this.todo = this.toDoService.getToDo(id);
    // this.itemEdit=this.todo;

  }

radioChangeHandler(event:any){
  console.log(event)
  this.selectedStatus=event.target.value;
  console.log(this.selectedStatus)
}
btnSave(item:HTMLInputElement)
{
  // this.itemEdited.emit({
  //   name: item.value,
  //   taskStatus: this.selectedStatus
  // });
  this.itemEdit.name = this.name.nativeElement.value;
  this.itemEdit.status= this.selectedStatus;
  // this.toDoService.ItemToEdit(item.value,this.selectedStatus);
}
btnCancel()
{
this.toDoService.CancelEdit();
}

}

