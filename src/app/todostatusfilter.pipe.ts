import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'todostatusfilter',
  
})
export class TodostatusfilterPipe implements PipeTransform {

  transform(value: any, showStatus:string): any {
    if(showStatus=="all") {
      return value;
    }

    console.log(showStatus,value)   
    const result = [];

    for (let toDo of value) {
      if (toDo.status==showStatus) {
       result.push(toDo);
      }
    }

    return result;
  }
}
